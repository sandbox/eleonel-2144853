################################
Theme Information
-------------------------------------------------------------------------------
INK is a flexible base theme for Drupal 7 based on the INK Framework. This is 
an unofficial Drupal theme base of INK framework (http://ink.sapo.pt/).

- Theme Version 1.0
- Compatible with Drupal 7.x
- Copyright (C) 2013-2014 Eleonel Basili. Some Rights Reserved.
- @license - GNU/GPL, http://www.gnu.org/licenses/gpl.html
- Author: Eleonel Basili
- Website: http://www.eleonelbasili.com.ar


################################
Folder Structure
-------------------------------------------------------------------------------
- README.txt
- ink: INK base theme
- ink_starter: INK starter sub-theme

################################
Installation
-------------------------------------------------------------------------------
From Demo profile package:
This installation profile includes the same demo content of official INK 
framework at: http://ink.sapo.pt/
Step 1: Extract demo profile package to your host.
Step 2: Navigate to the folder you have extracted the zip file and install
  ink profile.

Theme only installation:
Step 1: Extract included zip files INK into sites/all/themes folder.
Step 2: Install INK Theme first. Go to theme settings under "Appearence"
  section and install this theme via upload.
Step 3: Install your INK starter sub-theme. Go to theme settings under 
"Appearence" section and Install this theme via upload.
Step 4: Enable and set as default.

################################
Documentation
-------------------------------------------------------------------------------
Read the official documentation of INK Framework at 
http://ink.sapo.pt/getting_started
Drupal theming documentation in the Theme Guide: http://drupal.org/theme-guide
